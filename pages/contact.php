<?php 
/**
 * 	Template Name: Contact
 *
 *
*/
get_header(); // This fxn gets the header.php file and renders it ?>

<section class="contactsplash">
	<h1 class="heading">
		CONTACT US
	</h1>
</section>	
	<!--
	<div class="contactinfo">
		<div class="address">
			<h2>ADDRESS</h2>
			<h3>1040  Travis Street<br>Fort Wayne, IN. 46829</h3>
		</div>
		<div class="phone">
			<h2>PHONE</h2>
			<h3>1.260.234.1242</h3>
		</div>
	</div>
	-->

<section class="contactform">
	<?php echo do_shortcode("[formidable id=1]");?>
</div>	
	
<?php get_footer(); // This fxn gets the footer.php file and renders it ?>