<?php 
/**
 * 	Template Name: HOME PAGE
 *
 *	This page template has a sidebar built into it, 
 * 	and can be used as a home page, in which case the title will not show up.
 *
*/

get_header(); // This fxn gets the header.php file and renders it ?>

<!-- HERO CONTENT -->


<?php 
	// Splash Header
?>

<section id="splash">
	<div class="wrapper">
	<?php if( have_rows('splash') ): ?>
		<? while( have_rows('splash') ): the_row();
	
			// vars
			$headline = get_sub_field('headline');
			$headline_small = get_sub_field('headline_small');
			$text = get_sub_field('text');
			$images = get_sub_field('gallery');
			?>
			
			<div class="splash-container">
				<?php if( $headline ): ?>
					<div class="headline-container">
						<div class="splash-text">
							<h1>
								<?php if( $headline_small ): ?>
									<span><?php echo $headline_small; ?></span>
								<?php endif; ?>
								
								<?php echo $headline; ?>
							
							</h1>
						</div>
					</div>
				<?php endif; ?>
				
				<?php if( $text ): ?>
					<div class="content">
						<?php the_sub_field('text'); ?>
					</div>
				<?php endif; ?>
				
				<?php if( have_rows('buttons') ): ?>
					<div class="button-container">
						<?php while( have_rows('buttons') ): the_row(); 
							
							//vars
					        $text = get_sub_field('text');
							$link = get_sub_field('link');
						?>
							<a class="button" href="<?php echo $link; ?>"> <?php echo $text; ?></a>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>
				
			</div>
			
			<?php if ( $images ) : ?>
				<div id="slider-splash-background"<?php if ( is_array($images) && (count($images) > 1) ) : ?> class="owl-carousel"<?php endif; ?>>
					<?php foreach ( $images as $image ) : ?>
						<div class="slide" style="background-image: url('<?php echo $image['url']; ?>');"></div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		<?php endwhile; ?> 
	<?php endif; ?>
	</div>
</section>

<div class="homepage-index">

	
		<?php if( have_rows('cta_row') ): 
			while( have_rows('cta_row') ): the_row();
	
			// vars
			$headline = get_sub_field('headline');
			$subhead = get_sub_field('subhead');
			$content = get_sub_field('content');
			$background = get_sub_field('background');
			$image = get_sub_field('image');
			$buttons = get_sub_field('buttons');
		?>
		
		<div class="mission" style="background-image: url('<?php echo $background['url']; ?>');">
			
			<div class="cta-container">
				<h2><?php echo $headline; ?></h2>
				<p><?php echo $subhead; ?></p>
				<div class="cta-content"><?php echo $content; ?></div>
				
				<?php if( have_rows('buttons') ): ?>
					<div class="button-container">
						<?php while( have_rows('buttons') ): the_row(); 
							
							//vars
					        $text = get_sub_field('text');
							$link = get_sub_field('link');
						?>
							<a class="button" href="<?php echo $link; ?>"> <?php echo $text; ?></a>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>
			</div>
			
				<div class="cta-image">
					<img src="<?php echo $image['url']; ?>"
				</div>
			</div>
		</div>
		
		<?php endwhile; ?>
	<?php endif; ?>


	<!-- SHOP PAGES -->
	
	<?php if( have_rows('products') ): // Name from ACF Field Name ?>
	
		<h2 class="shop">SHOP</h2> 
	
		<div class="grid-container">
		
			<?php while( have_rows('products') ): the_row(); 
		
				// vars from the sub field names, change name of variable to same had sub field name
				$image = get_sub_field('image_background');
				$headline = get_sub_field('headline');
				$link = get_sub_field('link');
		
				?>
		
				<a class="cta_products" href="<?php echo $link; ?>" style="background: url(<?php echo $image; ?>); background-size: cover; 	
					background-position: center center;">
		
					<!-- <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" /> -->
					
					<p class="sorting"><?php echo $headline; ?></p>
		
				</a>
		
			<?php endwhile; ?>
	
		</div>
	
	<?php endif; ?>

</div>




	
	
<?php get_footer(); // This fxn gets the footer.php file and renders it ?>