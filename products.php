<?php 
/**
 * 	Template Name: Products
 *
 *	This page template has a sidebar built into it, 
 * 	and can be used as a home page, in which case the title will not show up.
 *
*/

get_header(); // This fxn gets the header.php file and renders it ?>


<?php
	$default = false;
	if ($_GET['id'] !== false) {
		$default = $_GET['id'];
	}
/*
	if ($default && !is_numeric($default)) {
		$default = false;
	}
*/

?>

<!-- Modal -->

<script>
	var slider;
		
	$( document ).ready(function() {
		$('#keepShopping').on("click", function() {
			$("#success").hide();
		});
	    $( ".productBlock" ).on( "click", function() {
		    $('#overFlow').fadeIn(100);
			toModal(this);
		});
		$('.close').click(function (event) {
			$("#productModal").hide();
			$('#overFlow').hide();
			$("body").attr("style","overflow-y:auto;");
		});
		$('#overFlow').click(function (event) {
			$("#productModal").hide();
			$('#overFlow').hide();
			$("body").attr("style","overflow-y:auto;");
		});
		$('#sizes').on('change', function (e) {
		    $('.modalButton').attr('href', '?add-to-cart=' + ($(this).find(':selected').attr('data-id')));
		    $('.modalPrice').text($(this).find(':selected').attr('data-price'));
		});
	});
	
	<?php if ($default): ?>
		$( document ).ready(function() {
			const prodBlock = $('#<?=$default?>')[0];
			toModal(prodBlock);
			$('#overFlow').fadeIn(100);
		});
	<?php endif; ?>
	
	function toModal(prodBlock) {
		if (slider) {
			slider.destroy();
		}
		$("#productModal > #lightSlider").empty();
		$("#productModal").fadeIn(100);
		$("body").attr("style","overflow-y:hidden;");
		$(".modalName").text(prodBlock.dataset.name);
		$(".modalPrice").html(JSON.parse(prodBlock.dataset.price));
		$(".modalDesc").html(JSON.parse(prodBlock.dataset.html));
		$('.modalImg').attr('src',prodBlock.dataset.imgsrc);
		$('.modalButton').attr('href', '?add-to-cart=' + prodBlock.dataset.id);
		$("#productModal > #lightSlider").html(prodBlock.dataset.gallery);
		$("#moreinfo").attr('href', prodBlock.dataset.link);
		if (prodBlock.dataset.options) {
			$('#sizes').html(prodBlock.dataset.options);
			$('#sizes').show();
		} else {
			$('#sizes').hide();
		}

		slider = $('#lightSlider').lightSlider({
		  gallery: true,
		  item: 1,
		  loop: true,
		  slideMargin: 0,
		});
		slider.refresh();

	}
	
	window.history.pushState(null,null,window.location.href.split('?')[0]);
		
</script>

<!-- Add to Cart -->

<?php if ($_GET['add-to-cart']): ?>
<div id="success">
	<div class="cartalert">
		<div class="success">
			<i class="fa fa-check-circle" aria-hidden="true" style="color: #fff"></i>
			<p><b>Item added to Cart</b></p>
			<p><i>Apply Promo Code at Checkout</i></p>
		</div>	
		<div class="cartcta">
			<button><a id="keepShopping">Continue Shopping</a></button>
			<button><a href="<?php get_site_url() ?>/cart/">View Cart</a></button>
		</div>
	</div>
</div>
<?php endif;?>

<!-- Product Header -->

<?php if( have_rows('productsplash') ): ?>
    <?php while( have_rows('productsplash') ): the_row(); 
		
        // Get sub field values.
        $leftimage = get_sub_field('leftimage');
        $rightimage = get_sub_field('rightimage');
        $productheading = get_sub_field('productheading');
        $collectioncta = get_sub_field('collectioncta');
     ?>
  
 	<section class="productsplash" style="background: black;">
		<h1 class="productheading"><?=$productheading?></h1>
		<div class="leftimage" style="background: url(<?php echo $leftimage['url'];?>) no-repeat; background-size: 			100%;"></div>
		<div class="rightimage" style="background: url(<?php echo $rightimage['url']; ?>) no-repeat; background-size: 100%;"></div>
	</section>
	
	<div class="productsubhead">
		<h2 class="collectioncta"><?=$collectioncta?></h2>
	</div>
    
    <?php endwhile; ?>
<?php endif; ?>

<!-- Product Header END -->


<div id="overFlow">
</div>

<div id="productModal">
	<div class="close"><i class="fal fa-times"></i></div>
	<ul id="lightSlider"></ul>
	<p class="modalName"></p>
	<p class="modalPrice"></p>
	
	<div class="productselect">
		<select id="sizes"></select>
<!-- <a id="moreinfo" href="" >LEARN MORE</a> -->
		<a class="modalButton" href="<?php get_site_url() ?>/cart/">Add To Cart</a>
	</div>	
	<div class="modalDesc"><hr width="1" size="500"></div>
	<section class="related products"></section>

</div>

<div class="products">
	

<?php 
	$args = array(
        'post_type'      => 'product',
        'posts_per_page' => 20,
        'category' => array( get_query_var('pagename') ),
    );
	$products = wc_get_products( $args );
	
		foreach($products as $key => $product) {
			$varID = $product->id;
			$price_html = false;
			$fields = get_fields($product->id);

			$clothingColors = "";
			if ($fields['color']){
				foreach ($fields['color'] as $color) {
					$clothingColors = $clothingColors . '<div class="producthex" style="width:5rem;background-color:'. $color['productcolor'] . '";> </div>';
				}
			}
			
			$value = get_post_meta($post_id, 'repeater_1_sub_field', true);
			$varihtml = $optionhtml = '';
			$id = false;
			if (!$product->is_type( 'simple' ) ) {
				$varihtml = 
				'<div class="productdetail">'
				. $clothingColors .
					'<div class="productsize">
						<div style="order: 1;"><ul style="margin: 0px;">
					</div>
				</div>';
				$optionhtml = '';
				foreach ($product->get_available_variations() as $key => $variation) {
					$price_html = $price_html ? $price_html : $variation['price_html'];
					$keyLetter = $variation['attributes']['attribute_sizes'][0];
					$keyLetter = $keyLetter == 'X' ? $variation['attributes']['attribute_sizes'][0] . $variation['attributes']['attribute_sizes'][1] : $keyLetter;
					$is = ($variation['is_in_stock'] ? '' : 'color: #AFAFAF; font-weight:400;');
					$varihtml .= '<li style="display:inline; ' . $is . '">' 
									. $keyLetter . '</li>';
					if ($variation['is_in_stock']) {
						$optionhtml .= '<option data-id="'.$variation['variation_id'].'" data-price="'.htmlentities($variation['display_price']).'">' . $variation['attributes']['attribute_sizes'] . '</option>';
						if (!$id) {
							$id = $variation['variation_id'];
						}
					}
				}
				$varihtml .= '</ul></div>';
			} else {
				$id = $product->id;
			}

			$price = "";
			$imageThumb = get_the_post_thumbnail_url($product->id);
			$urlId = str_replace(' ','_',$product->slug);
			$galImgs = '<li class="slider-list lslide" data-thumb="' . get_the_post_thumbnail_url($product->id) . '"><img class="slider-image" src="' . get_the_post_thumbnail_url($product->id) . '" /></li>';
			foreach($product->get_gallery_image_ids() as $attachmentId) {
				$galImgs .= '<li class="slider-list lslide" data-thumb="' . wp_get_attachment_url($attachmentId) . '"><img class="slider-image" src="' . wp_get_attachment_url($attachmentId) . '" /></li>';  
			}

			if (isset($fields[strtolower($productheading) . '_photos'])) {
				$imageThumb = $fields[strtolower($productheading) . '_photos'][0]['men_pic']['url'];
				$galImgs = '';
				foreach($fields[strtolower($productheading) . '_photos'] AS $photo) {
				$galImgs .= '<li class="slider-list lslide" data-thumb="' . $photo['men_pic']['url'] . '"><img class="slider-image" src="' . $photo['men_pic']['url'] . '" /></li>';
				}
			}

			if (isset($fields[strtolower($productheading) . '_description'])) {
				$description = $fields[strtolower($productheading) . '_description'];
			} else {
				$description = $product->description;
			}


		  	echo '
	        	<div class="productBlock" id="' . str_replace(["'",'(',')'],['','',''],$urlId) . '" 
		        	data-gallery="' . htmlspecialchars($galImgs) . '" 
		        	data-name="' . $product->name . '"
		        	data-price="' . htmlentities(json_encode($price_html, JSON_UNESCAPED_SLASHES)) . '"
		        	data-imgsrc="' . $imageThumb . '"
		        	data-id="' . $id . '"
		        	data-html="' . htmlentities(json_encode($description, JSON_UNESCAPED_SLASHES)) . '"
		        	data-options="'.htmlentities($optionhtml).'"
		        	data-link="' . get_permalink($product->id) . '"
	        	>
	        		<a>
	        			<img class="productImage" src=' . $imageThumb . '></img>
	        		</a>
	        		<p class="productPrice">'. ($price_html ? $price_html : $product->price).'</p>
					<p class="productName">' . $product->name . '</p>
					'.$varihtml.'
	        	</div>';  
    }	


    wp_reset_query();	
?>

</div>


<?php get_footer(); // This fxn gets the footer.php file and renders it ?>