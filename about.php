<?php 
/**
 * 	Template Name: About
 *
 *	This page template has a sidebar built into it, 
 * 	and can be used as a home page, in which case the title will not show up.
 *
*/

get_header(); // This fxn gets the header.php file and renders it ?>


<div class="sub-menu">
	<div class="cta filter">			
	<a class="blue-btn btn" data-filter="about" href="#" role="button">About</a>
	<a class="blue-btn btn" data-filter="meettheteam" href="#" role="button">Meet The Team</a>
	<a class="blue-btn btn" data-filter="team" href="#" role="button">Join</a>
  	
	</div>
</div>


<div class="boxes">

<?php if( have_rows('about') ): 
	while( have_rows('about') ): the_row(); ?>
	
	<div data-cat="meettheteam" class="test">
		<div class="about-members wrapper">
		
			<?php if( have_rows('members') ): 
				while( have_rows('members') ): the_row();
				
				//vars
					$image = get_sub_field('image');
					$name = get_sub_field('name');
					$title = get_sub_field('title');
					$content = get_sub_field('content');
				?>
				
				<div class="members" style="background-image: url('<?php echo $image['url']; ?>');">
					
					<div class="opacity-target">
						
						<div class="members-container">
								
							<p class="members-title"><?php echo $title; ?></p>
							
							<p class="members-content"><?php echo $content; ?></p>
						
						</div>
						
					</div>
					
					<div class="member-info">
						<p class="member-name"><?php echo $name; ?></p>
						
						<?php if( have_rows('social') ): 
							while( have_rows('social') ): the_row(); 
							
							//vars
								$handle = get_sub_field('handle');
								$link = get_sub_field('link');
							?>
							
							<a href="<?php echo $link; ?>"><?php echo $handle; ?></a>
							
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
					
				</div>
				
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
	
	<div data-cat="about" class="meettheteam wrapper">
	
		<?php if( have_rows('meet_the_team') ): 
			while( have_rows('meet_the_team') ): the_row();
			
			//vars
				$image_one = get_sub_field('image_one');
				$headline_one = get_sub_field('headline_one');
				$content_one = get_sub_field('content_one');
				$image_two = get_sub_field('image_two');
				$headline_two = get_sub_field('headline_two');
				$content_two = get_sub_field('content_two');			
			?>
			
			<div class="mtt-container">
				
				<div class="mtt">
				
					<div class="mtt-image">
						<img src="<?php echo $image_one['url']; ?>">
					</div>
					
					<div class="content-container">
						<p class="mtt-headline"><?php echo $headline_one; ?></p>
						<div class="mtt-content"><?php echo $content_one; ?></div>
					</div>
				</div>
				
				<div class="mtt">
				
					<div class="mtt-image">
						<img src="<?php echo $image_two['url']; ?>">
					</div>
					
					<div class="content-container">
						<p class="mtt-headline"><?php echo $headline_two; ?></p>
						<div class="mtt-content"><?php echo $content_two; ?></div>
					</div>					
				
				</div>
				
			</div>
			
			<?php endwhile; ?>
		<?php endif; ?>
		
		<?php if( have_rows('about_cta') ): 
			while( have_rows('about_cta') ): the_row();
			
			//vars
				$background_image = get_sub_field('background_image');
				$cta_headline = get_sub_field('cta_headline');
		?>
		
			<div class="about-cta" style="background: url(<?php echo $background_image['url'];?>) no-repeat; background-size: cover;">
				
				<div class="wrapper">
					<p class="about-headline"><?php echo $cta_headline; ?></p>
				
					<?php if( have_rows('about_social') ): ?>
					
					<div class="about-social-container">
						<?php while( have_rows('about_social') ): the_row();
						
						//vars
							$font_awesome = get_sub_field('font_awesome');
							$link = get_sub_field('link');
					?>
						
						<a href="<?php echo $link; ?>"><i class="<?php echo $font_awesome; ?>" target="_blank"></i></a>
					
						<?php endwhile; ?>
						</div>
					<?php endif; ?>	
				</div>
				
			</div>
			
			<?php endwhile; ?>
		<?php endif; ?>
		
	</div>
	
	<div data-cat="team" class="join wrapper">
	
		<?php if( have_rows('join') ): 
			while( have_rows('join') ): the_row();
			
			//vars
				$image_join = get_sub_field('image_join');
				$form = get_sub_field('form');		
			?>
			
			<div class="join-container">
				
				<div class="join-image">
					<img src="<?php echo $image_join['url']; ?>">
				</div>
				
				<div class="join-form">
					<?php echo FrmFormsController::get_form_shortcode( array( 'id' => 4, 'title' => false, 'description' => false ) ); ?>
				</div>
				
			</div>
			
			<?php endwhile; ?>
		<?php endif; ?>
		
	</div>


	
	<?php endwhile; ?>
<?php endif; ?>

</div>


	

<?php get_footer(); // This fxn gets the footer.php file and renders it ?>