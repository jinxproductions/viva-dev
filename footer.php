<?php
	/*-----------------------------------------------------------------------------------*/
	/* This template will be called by all other template files to finish 
	/* rendering the page and display the footer area/content
	/*-----------------------------------------------------------------------------------*/
?>

</main><!-- / end page container, begun in the header -->

<footer class="site-footer">
	<section id="footer-content">
		<div class="wrapper">
			
			 <?php if ( have_rows('footer', 'options') ) : ?> <!-- Containing ACF ------>
		    	<div class="footer-container">
					<?php while ( have_rows('footer', 'options') ) : the_row(); 
						$socials = get_sub_field('socials');
						$newsletter = get_sub_field('newsletter');
						$company = get_sub_field('company');
					?>
					
<!-- Group #1 ------>	

					<?php if ( have_rows('company') ) : ?> 
						<?php while ( have_rows('company') ) : the_row(); 
							$logo = get_sub_field('logo');
							$slogan = get_sub_field('slogan');
						?>
						
						<div id="companyslogan">
							<img src="<?php echo $logo['url'];?>" alt="<?php echo $logo ['alt'];?>">	
							<?php the_sub_field('slogan'); ?>			
						</div>
						
						<?php endwhile; ?>
					<?php endif; ?>	
					
<!-- Group #2 ------>						
						<div id="footer-links">
							<ul>
								<h4>Pages</h4>
								<?php wp_nav_menu ( array( 'theme_location' => 'Pages' ) ); ?>
							</ul>
							<ul>
								<h4>Help</h4>
								<?php wp_nav_menu ( array( 'theme_location' => 'Help' ) ); ?>
							</ul>
							<ul>
								<h4>Account</h4>
								<?php wp_nav_menu ( array( 'theme_location' => 'Account' ) ); ?>
							</ul>
						</div>
		
					
<!-- Group #3 ------>	
					<div class="social-container">
						<h4>Stay Connected</h4>
						<?php if ( have_rows('socials') ) : ?> 
							<div id="socialmedia">
							<?php while ( have_rows('socials') ) : the_row(); 
								$text = get_sub_field('text');
								$link = get_sub_field('link');
							?>
								<a href="<?php the_sub_field('link'); ?>" target="_blank">
									<i class="<?php echo $text; ?>"></i>
								</a>
							
							<?php endwhile; ?>
							</div>
						<?php endif; ?>	
					</div>	
					
<!-- Group #4 ------>		
						
						<div id="newsletter">
							<?php the_sub_field('newsletter'); ?>						
						</div>	
						
					
					<?php endwhile; ?>	
			    </div>	<!-- Containing Div ------>		
			<?php endif; ?>	<!-- Containing ACF ------>
			
		</div>	
	</section>
	
<!-- Disclaminer Section ------>
	
	<section id="footer-disclaimer">
		<div class="wrapper">
			
			<?php if ( have_rows('disclaimer', 'options') ) : ?> 
				<div id="disclaimer">
				<?php while ( have_rows('disclaimer', 'options') ) : the_row(); 
					$copyright = get_sub_field('copyright');
					$terms = get_sub_field('terms');
				?>
				
					<div id="copyright"><?php the_sub_field('copyright'); ?></div>
					<div id="terms"><?php the_sub_field('terms'); ?></div>
				
				<?php endwhile; ?>
				</div>
			<?php endif; ?>	
			
		</div>
	</section>
</footer>


<?php wp_footer(); 
// This fxn allows plugins to insert themselves/scripts/css/files (right here) into the footer of your website. 
// Removing this fxn call will disable all kinds of plugins. 
// Move it if you like, but keep it around.
?>

</body>
</html>
