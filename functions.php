<?php
	/*-----------------------------------------------------------------------------------*/
	/* This file will be referenced every time a template/page loads on your Wordpress site
	/* This is the place to define custom fxns and specialty code
	/*-----------------------------------------------------------------------------------*/

// Define the version so we can easily replace it throughout the theme
define( 'NAKED_VERSION', 1.0 );

/*-----------------------------------------------------------------------------------*/
/*  Set the maximum allowed width for any content in the theme
/*-----------------------------------------------------------------------------------*/
if ( ! isset( $content_width ) ) $content_width = 900;

/*-----------------------------------------------------------------------------------*/
/* Add Rss feed support to Head section
/*-----------------------------------------------------------------------------------*/
add_theme_support( 'automatic-feed-links' );

/*-----------------------------------------------------------------------------------*/
/* Add post thumbnail/featured image support
/*-----------------------------------------------------------------------------------*/
add_theme_support( 'post-thumbnails' );

/*-----------------------------------------------------------------------------------*/
/* Register main menu for Wordpress use
/*-----------------------------------------------------------------------------------*/
register_nav_menus( 
	array(
		'primary'	=>	__( 'Primary Menu', 'naked' ), // Register the Primary menu
		// Copy and paste the line above right here if you want to make another menu, 
		// just change the 'primary' to another name
	)
);

/*-----------------------------------------------------------------------------------*/
/* Adds more Menus
/*-----------------------------------------------------------------------------------*/

register_nav_menus( array(  
	'Pages' => __('Pages'),
	'Help' => __('Help'),
	'Account' => __('Account'),
) ); 

/*-----------------------------------------------------------------------------------*/
/* Activate sidebar for Wordpress use
/*-----------------------------------------------------------------------------------*/
function naked_register_sidebars() {
	register_sidebar(array(				// Start a series of sidebars to register
		'id' => 'sidebar', 					// Make an ID
		'name' => 'Sidebar',				// Name it
		'description' => 'Take it on the side...', // Dumb description for the admin side
		'before_widget' => '<div>',	// What to display before each widget
		'after_widget' => '</div>',	// What to display following each widget
		'before_title' => '<h3 class="side-title">',	// What to display before each widget's title
		'after_title' => '</h3>',		// What to display following each widget's title
		'empty_title'=> '',					// What to display in the case of no title defined for a widget
		// Copy and paste the lines above right here if you want to make another sidebar, 
		// just change the values of id and name to another word/name
	));
} 
// adding sidebars to Wordpress (these are created in functions.php)
add_action( 'widgets_init', 'naked_register_sidebars' );

/*-----------------------------------------------------------------------------------*/
/* Enqueue Styles and Scripts
/*-----------------------------------------------------------------------------------*/

function theme_scripts()  { 

	// get the theme directory style.css and link to it in the header
	wp_enqueue_style('style.css', getFileWithMtime('/style.css'));
	
	// Owl Carousel
	wp_register_script( 'owlcarousel_scripts', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js', array('jquery'), 'all', true );
	wp_enqueue_script( 'owlcarousel_scripts');
	wp_enqueue_style('owlcarousel_styles', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css');
	
	// add script.js scripts
    wp_register_script( 'mobilemenu', (get_stylesheet_directory_uri() . '/js/script.js'), array('jquery'), 'all', true );
    wp_enqueue_script( 'mobilemenu');
  
}
add_action( 'wp_enqueue_scripts', 'theme_scripts' ); // Register this fxn and allow Wordpress to call it automatcally in the header

/*-----------------------------------------------------------------------------------*/
/* MENU HEADER DONT DELETE
/*-----------------------------------------------------------------------------------*/

function wp_nav_menu_no_ul()
{
    $options = array(
        'echo' => false,
        'container' => false,
        'theme_location' => 'primary',
        'fallback_cb'=> 'default_page_menu'
    );

    $menu = wp_nav_menu($options);
    echo preg_replace(array(
        '#^<ul[^>]*>#',
        '#</ul>$#'
    ), '', $menu);

}

function default_page_menu() {
   wp_list_pages('title_li=');
}

function custom_wc_ajax_variation_threshold( $qty, $product ) { return 100; } add_filter( 'woocommerce_ajax_variation_threshold', 'custom_wc_ajax_variation_threshold', 100, 2 );

add_filter( 'wc_product_has_unique_sku', '__return_false', PHP_INT_MAX );

/**
 * append modify time to file name
 * for cache busting purposes
 */
function getFileWithMtime($pathFromRoot) {
    $serverPath = get_stylesheet_directory() . $pathFromRoot;
    if ( file_exists ( $serverPath ) ) {
        $modifyTime = filemtime($serverPath);
        $pathFromRoot .= '?v=' . $modifyTime;
        $fileURL = get_stylesheet_directory_uri() . $pathFromRoot;
        return $fileURL;
    }
    return '';
}

// remove "downloads" link from my account pages
add_filter( 'woocommerce_account_menu_items', 'my_account_menu', 999 );

function my_account_menu( $items ) {
    unset($items['downloads']);
    return $items;
}


/*-----------------------------------------------------------------------------------*/
/* ADDS OPTIONS TAB
/*-----------------------------------------------------------------------------------*/

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}
