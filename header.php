<?php
	/*-----------------------------------------------------------------------------------*/
	/* This template will be called by all other template files to begin 
	/* rendering the page and display the header/nav
	/*-----------------------------------------------------------------------------------*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

<meta name="google-site-verification" content="12sUoHDr2zC_g-iJoyhguD6QvfUnUud3tKFk9A3HBpA" /> <!-- Google -->
<meta name="p:domain_verify" content="016eaeb4698b94235d99f73231271d5d"/> <!-- Pinterest -->
<meta name="msvalidate.01" content="14AB85640E4CD1775DF2D4891EC4F071" /> <!-- Bing -->
<meta name="facebook-domain-verification" content="vfpq0xqxigzvlqbd2z1dfx0m5w7cq0" /> <!-- Facebook -->

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '732775227544736');
  fbq('track', 'PageView');
</script>

<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=732775227544736&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
	
<!-- Google Tag Manager -->
<script>
	(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5FQL68T');
</script>
<!-- End Google Tag Manager -->
<!-- CONTENT -->
<link href="https://fonts.googleapis.com/css?family=Barlow+Condensed|Montserrat&display=swap" rel="stylesheet">

<!-- FontAwesome -->
<script src="https://kit.fontawesome.com/1bbee72024.js" crossorigin="anonymous"></script>


<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title>
	<?php bloginfo('name'); // show the blog name, from settings ?> | 
	<?php is_front_page() ? bloginfo('description') : wp_title(''); // if we're on the home page, show the description, from the site's settings - otherwise, show the title of the post or page ?>
</title>


<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<link type="text/css" rel="stylesheet" href="https://sachinchoolur.github.io/lightslider/dist/css/lightslider.css" />


<!--JS Needed for modules -->
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="https://sachinchoolur.github.io/lightslider/dist/js/lightslider.js"></script>

<?php // We are loading our theme directory style.css by queuing scripts in our functions.php file, 
	// so if you want to load other stylesheets,
	// I would load them with an @import call in your style.css
?>

<?php wp_head(); 
// This fxn allows plugins, and Wordpress itself, to insert themselves/scripts/css/files
// (right here) into the head of your website. 
// Removing this fxn call will disable all kinds of plugins and Wordpress default insertions. 
// Move it if you like, but I would keep it around.
?>

</head>

<?php
$bodyClasses = [];
$urlData = parse_url($_SERVER['REQUEST_URI']);
if ( ( $urlData['path'] == '/my-account/' && !is_user_logged_in() )
        || $urlData['path'] == '/my-account/lost-password/' ) {
    array_push($bodyClasses, 'woocommerce-no-flex');
}
?>

<body
	<?php body_class($bodyClasses);
	// This will display a class specific to whatever is being loaded by Wordpress
	// i.e. on a home page, it will return [class="home"]
	// on a single post, it will return [class="single postid-{ID}"]
	// and the list goes on. Look it up if you want more.
	?>
>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5FQL68T"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<div class="topcallout"> 
	 <?php if ( have_rows('header_callout', 'options') ) : ?>	 	
	    	
		<?php while ( have_rows('header_callout', 'options') ) : the_row(); 
			$text = get_sub_field('text');
		?>
		<a style="text-decoration:none;" target="_blank"><?php echo $text; ?></a>
	
	<?php endwhile; ?>
<?php endif; ?>

</div>

 <header class="header">
		<div class="wrapper">
			<nav class="nav-full">
				
				<a class="site-icon" href="/home">
					<?php echo file_get_contents(get_template_directory_uri() . "/images/vivalifestylelogowhite.svg"); ?>
				</a>

				<div class="menu">
					<ul><?php  wp_nav_menu_no_ul(); ?> </ul>
				</div>
			
				<div class="account-info">
					<a class="account-icon" href="/account"><i class="fas fa-user-circle"></i></a>
					<a class="cart" href="<?php echo wc_get_cart_url(); ?>"> 
						<i class="fa fa-shopping-cart"></i>
						<p class="cartnumber">
							<?php echo sprintf (_n( '%d', '%d', WC()->cart->cart_contents_count ), WC()->cart->cart_contents_count ); ?>
						</p>
					</a>	 	
				</div>
				
			</nav>
		</div>
		
		<!-- Navigation for Mobile ------->
		
		<nav class="mobile-nav">
			<a class="mobile-nav-trigger" href="#">
				<button class="hamburger hamburger--collapse" type="button">
				  <span class="hamburger-box">
				    <span class="hamburger-inner" id="hamber1"></span>
				  </span>
				</button>
			</a>
			
			<div class="hamburger-menu"> <!-- Content within Hamburger dropdown -->
				<div class="wrapper">
					<div class="account-buttons">
						<button id="signup" class="button-primary">Sign Up</button>
						<button id="login">Login In</button>
					</div>
					
					<ul id="hamburger-links">
						<?php  wp_nav_menu_no_ul(); ?>	
						<?php wp_nav_menu ( array( 'theme_location' => 'secondary' ) ); ?>
					</ul>
				</div> <!-- Wrapper ------>
			</div>
			
			<div class="siteicon">
				<a href="/home"><?php echo file_get_contents(get_template_directory_uri() . "/images/vivalifestyleicon.svg"); ?></a>
			</div>
			
			<a class="cart" href="<?php echo wc_get_cart_url(); ?>"> 
				<p class="shop-icon"></p>
				<div class="cartnumber">
					<p>
					<?php echo sprintf (_n( '%d', '%d', WC()->cart->cart_contents_count ), WC()->cart->cart_contents_count ); ?>
					</p>
				</div>
			</a>
		</nav>	
		
		
</header>



<div id="backgroundoverlay">
	
</div>



<div id="newsmodal">
	<div class="modalcontent">
		<div class="news-top">
			<a href="#" class="js-close-modal"><i class="fa fa-times" aria-hidden="true"></i></a>
			<div class="logoicon">
			<?php echo file_get_contents(get_template_directory_uri() . "/images/vivalifestyleicon.svg"); ?>
			</div>
			<p>2020 Collection</p>
		</div>
		<div class="news-bottom">
			<div class="cta-news">
				<p>
				<span>Free Shipping</span>
				On your first orders
				</p>
				<div class="emailsender">
<?php echo FrmFormsController::get_form_shortcode( array( 'id' => 2, 'title' => false, 'description' => false ) ); ?>					
				</div>  
				<p class="news-disclaimer">By signing up, you are agreeing to receive - news, exclusive offers / promos, product alerts, and other information regarding Viva Lifestyle. 
					
				</p>
				</div> 
			</div>
	</div>
</div> 



<script>

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

	
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
	
setTimeout(function(){
	console.log("test");
	if (getCookie("modalCookie") === "visit"){
		
	}
	else {
		document.getElementById("newsmodal").style.display = "flex"; 
		$('.modalcontent').show();
		document.getElementById("backgroundoverlay").style.display = "flex";
		setCookie("modalCookie", "visit", 20);
		
	}
}, 10000); 
 
 $(".js-open-modal").click(function(){
  $("#newsmodal").addClass("visible");
  $(".js-open-modal").fadeIn(6000);
});
$(".js-close-modal").click(function(){
	document.getElementById("newsmodal").style.display = "none";
	document.getElementById("backgroundoverlay").style.display = "none";
});

$("#backgroundoverlay").click(function(){
	document.getElementById("newsmodal").style.display = "none";
	document.getElementById("backgroundoverlay").style.display = "none";
});

$(document).click(function(event) {
	
  //if you click on anything except the modal itself or the "open modal" link, close the modal
  if (!$(event.target).closest("#newsmodal").length) {
		document.getElementById("newsmodal").style.display = "none";
		document.getElementById("backgroundoverlay").style.display = "none";
  }
});
 </script> 



<main class="main-fluid"><!-- start the page containter -->

