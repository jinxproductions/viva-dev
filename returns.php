<?php 
/**
 * 	Template Name: Returns
 *
 *	This page template has a sidebar built into it, 
 * 	and can be used as a home page, in which case the title will not show up.
 *
*/

get_header(); // This fxn gets the header.php file and renders it ?>

<section class="returnsplash">
	<h1 class="heading">
		RETURNS
	</h1>
</section>	



<section class="returnform">
	<?php echo do_shortcode("[formidable id=3]");?>
</div>	

<?php get_footer(); // This fxn gets the footer.php file and renders it ?>